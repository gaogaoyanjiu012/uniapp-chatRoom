import wsh from './WebSocketHelper.js';
import msg from './Message.js';
import helper from './helper.js'
import config from './config.js'

var WSHelper = null;

/** vue 对象 */
function AppChatInit(v) {

	v.messages = helper.GetSesstion();

	uni.getSystemInfo({
		success: function(res) {

			var clientInfo = plus.push.getClientInfo();

			var clientid = clientInfo.clientid;

			if (!clientInfo.clientid) {

				clientid = uni.getStorageSync('clientid');

				if (!clientid) {
					setTimeout(function() {
						AppChatInit(v);
					}, 500);
					return;
				}


			} else {
				uni.setStorageSync('clientid', clientInfo.clientid);
				clientid = clientInfo.clientid;
			}

			v.clientid = clientid;

			WSHelper = new wsh.WebSocketHelper({
				uuid: clientid,
				projectToken: config.Project.Token,
				parameter: JSON.stringify({
					client: res.platform
				}),
				url: ''
			}, {
				connect: uni.connectSocket,
				onopen: uni.onSocketOpen,
				onmessage: uni.onSocketMessage,
				onclose: uni.onSocketClose,
				send: uni.sendSocketMessage
			}, v);

			WSHelper.onopen(() => {
				console.log("onopen")
			});

			WSHelper.onnetwork((e) => {
				console.log('onnetwork:' + e)
			});

			WSHelper.onclose(() => {
				console.log("onclose")
			});

			WSHelper.onmessage((result) => {

				switch (result.type) {

					case 'length':
						msg.Task(v, result.type, result.length);
						break;

					case 'offline':
					case 'userlogin':
						msg.Task(v, result.type, result.date);
						break;

					case 'current':
						msg.Task(v, result.type, result);
						break;

					case 'clients':
						for (var i = 0; i < result.date.length; i++) {

							var item = result.date[i];

							if (item.Value.User.Parameter) {

								var reg = new RegExp("'", "g")
								var parameter = JSON.parse(item.Value.User.Parameter.replace(reg, '"'));

								if (parameter && parameter.client == res.platform) {
									WSHelper.OutLogin(item.Value['ServiceToken']);
								}
							}
						}
						break;

					default:
						msg.Task(v, result.type, result);
						break;

				}

			});

		}
	});
}

export default {
	Init: function(v) {
		setTimeout(function() {
			AppChatInit(v);
		}, 1000);
	},
	Task: function(v, temp) {

		var messageType = 0;
		var parameter = {
			type: temp.msg.type,
			length: temp.msg.content.length
		};

		if (v.sesstion.indexOf('group_') > -1) {
			messageType = 1;
			parameter.title = temp.taskUserInfo.userName;
			parameter.url = temp.taskUserInfo.headImg;
		}

		WSHelper.send({
			Content: temp.msg.content.text,
			MessageKey: v.sesstion,
			MessageType: messageType,
			Parameter: JSON.stringify(parameter)
		}, function() {
			console.log('发送成功')
		}, function() {
			console.log('发送失败')

		});

		msg.Send(v, temp);
	},

	TaskLogin: function(v, temp) {

		WSHelper.send({
			Content: temp.content,
			MessageKey: temp.sesstion,
			MessageType: 0,
			Parameter: JSON.stringify({
				type: 'login'
			})
		}, function() {
			console.log('发送成功')
		}, function() {
			console.log('发送失败')
		});

	},

	Reload: function() {
		if (WSHelper) WSHelper.showReload();
	}
}
