export default[
   {
      "name": "前端开发",
      "foods": [
          {
            "name": "HTML/CSS",
            "key": "HTML/CSS",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "JavaScript",
            "key": "JavaScript",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "vue.js",
            "key": "vue.js",
           "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "nodeJs",
            "key": "nodeJs",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "react",
            "key": "react",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "Jquery",
            "key": "Jquery",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "Sass/Less",
            "key": "Sass/Less",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "WebApp",
            "key": "WebApp",
            "icon": "/static/img/code-4.png",
            "cat": 10
          },
          {
            "name": "小程序",
            "key": "小程序",
            "icon": "/static/img/code-4.png",
            "cat": 10
          }
      ]
   },
   {
    "name": "后端开发",
    "foods": [
        {
          "name": "Java",
          "key": "Java",
           // "icon": "/static/img/java-3.png",
		  "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "SpringBoot",
          "key": "SpringBoot",
          "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "SpringCloud",
          "key": "SpringCloud",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "Python",
          "key": "Python",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "Django",
          "key": "Django",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "Go",
          "key": "Go",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "PHP",
          "key": "PHP",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "C",
          "key": "C",
           "icon": "/static/img/code-4.png",
          "cat": 6
        },
        {
          "name": "C++",
          "key": "C++",
          "icon": "/static/img/code-4.png",
          "cat": 6
        }
        ]
    },
    {
        "name": "移动开发",
        "foods": [
            {
                "name": "安卓",
                "key": "安卓",
                "icon": "/static/img/code-4.png",
                "cat": 3
            },
            {
                "name": "IOS",
                "key": "IOS",
                "icon": "/static/img/code-4.png",
                "cat": 3
            },
            {
                "name": "嵌入式",
                "key": "嵌入式",
                "icon": "/static/img/code-4.png",
                "cat": 3
            }

        ]
    },
    {
        "name": "计算机基础",
        "foods": [
            {
              "name": "计算机网络",
              "key": "计算机网络",
              "icon": "/static/img/code-4.png",
              "cat": 4
            },
            {
                "name": "算法与数据结构",
                "key": "算法与数据结构",
                "icon": "/static/img/code-4.png",
                "cat": 4
            },
            {
                "name": "数学",
                "key": "数学",
                "icon": "/static/img/code-4.png",
                "cat": 4
            },
           
        ]
    },
    {
        "name": "前沿技术",
        "foods": [
            {
                "name": "微服务",
                "key": "微服务",
                "icon": "/static/img/code-4.png",
                "cat": 12
            },
            {
                "name": "区块链",
                "key": "区块链",
                "icon": "/static/img/code-4.png",
                "cat": 12
            },
            {
                "name": "以太坊",
                "key": "以太坊",
               "icon": "/static/img/code-4.png",
                "cat": 12
            },
            {
                "name": "深度学习",
                "key": "深度学习",
                "icon": "/static/img/code-4.png",
                "cat": 12
            },
            {
                "name": "机器学习",
                "key": "机器学习",
                "icon": "/static/img/code-4.png",
                "cat": 12
            }
        ]
    },
    {
        "name": "云计算&大数据",
        "foods": [
            {
                "name": "大数据",
                "key": "大数据",
                "icon": "/static/img/code-4.png",
                "cat": 5
            },
            {
                "name": "Hadoop",
                "key": "Hadoop",
                "icon": "/static/img/code-4.png",
                "cat": 5
            },
			{
			    "name": "Spark",
			    "key": "Spark",
			    "icon": "/static/img/code-4.png",
			    "cat": 5
			}
        ]
    },
    {
        "name": "运维&测试",
        "foods": [
            {
                "name": "自动化运维",
                "key": "自动化运维",
                "icon": "/static/img/code-4.png",
                "cat": 8
            },
            {
                "name": "接口测试",
                "key": "接口测试",
                "icon": "/static/img/code-4.png",
                "cat": 8
            }
        ]
    },
    {
        "name": "数据库",
        "foods": [
            {
              "name": "MySQL",
              "key": "MySQL",
             "icon": "/static/img/code-4.png",
              "cat": 2  
            },
            {
                "name": "Oracle",
                "key": "Oracle",
                "icon": "/static/img/code-4.png",
                "cat": 2  
            }
        ]
    },
    {
        "name": "UI设计&多媒体",
        "foods": [
            {
                "name": "设计工具",
                "key": "设计工具",
                "icon": "/static/img/code-4.png",
                "cat": 0
            },
            {
                "name": "APP设计",
                "key": "APP设计",
                "icon": "/static/img/code-4.png",
                "cat": 0
            },
            {
                "name": "产品交互",
                "key": "产品交互",
                "icon": "/static/img/code-4.png",
                "cat": 0
            }
        ]
    },
    {
        "name": "游戏",
        "foods": [
            {
                "name": "Unity 3D",
                "key": "Unity 3D",
                "icon": "/static/img/code-4.png",
                "cat": 11
            },
            {
                "name": "Cocos2d-x",
                "key": "Cocos2d-x",
                "icon": "/static/img/code-4.png",
                "cat": 11
            }
        ]
    },
    {
        "name": "全栈工程师",
        "foods": [
            {
                "name": "javafullState",
                "key": "javafullState",
                "icon": "/static/img/code-4.png",
                "cat": 7
            },
            // {
            //     "name": "办公文具",
            //     "key": "办公文具",
            //     "icon": "/static/img/code-4.png",
            //     "cat": 7
            // }
        ]
    }
]