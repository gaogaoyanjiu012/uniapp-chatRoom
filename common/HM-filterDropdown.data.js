// 数据格式,数据中只需要包含以下字段和数据格式,开发者可以添加字段,比如id等等,不影响组件显示,组件的返回结果是以菜单数组下标形式返回,开发者可根据返回的下标获取所选中的菜单
/*
[
	{
		"name":"",	//字符串类型 选填项 菜单名称,如不填,则取第一个子菜单的name值,filter类型则将设置为"筛选"
		"type":""	//字符串类型 必填项 可取值 hierarchy/filter  hierarchy单/多层级菜单(最多三级); filter筛选多选菜单
		"submenu":[	//对象数组类型 必填项 子菜单数据
			{
				"name":"",	//字符串类型 必填项 菜单名称
				"submenu":[	//对象数组类型 必填项 子菜单数据
					{
						"name":"",	//字符串类型 必填项 菜单名称
						"submenu":[	//对象数组类型 必填项 子菜单数据 filter类型无效 
							{
								"name":"",	//字符串类型 必填项 菜单名称 hierarchy类型层级最多到此
							}
						]
					}
				]
			}
		]
	}
]
*/
export default [{
		// "name":'全部分类',
		"type": 'hierarchy',
		"submenu": [
			{
				"name": '全部分类',
				"submenu": [{
					"name": "全部分类"
				}]
			},
			{
				"name": '前端开发',
				"submenu": [
					// {
					// 	"name": "全部"
					// },
					{
						"name": "全部",
						"submenu": [
						{
						  "name": "HTML/CSS",
						},
						{
						  "name": "JavaScript",
						},
						{
						  "name": "vue.js",
						},
						{
						  "name": "nodeJs",
						},
						{
						  "name": "react",
						},
						{
						  "name": "Jquery",
						},
						{
						  "name": "Sass/Less",
						},
						{
						  "name": "WebApp",
						},
						{
						  "name": "小程序",
						}
					   ]
					}]
					},
			{
				"name": '后端开发',
				"submenu": [{
					"name": "全部",
					"submenu": [
						{
						  "name": "Java",
						},
						{
						  "name": "SpringBoot",
						},
						{
						  "name": "SpringCloud",
						},
						{
						  "name": "Python",
						},
						{
						  "name": "Django",
						},
						{
						  "name": "Go",
						},
						{
						  "name": "PHP",
						},
						{
						  "name": "C",
						},
						{
						  "name": "C++",
						}
					]
				}]
			},
			{
				"name": '移动开发',
				"submenu": [{
					"name": "全部",
					"submenu": [
						{
						    "name": "安卓",
						},
						{
						    "name": "IOS",
						},
						{
						    "name": "嵌入式",
						}
					]
				}]
			},
			{
				"name": '计算机基础',
				"submenu": [{
					"name": "全部",
					"submenu": [
						{
						  "name": "计算机网络",
						},
						{
						  "name": "算法与数据结构",
						},
						{
						  "name": "数学",
						},
					]
				}]
			},
			{
				"name": '数据库',
				"submenu": [{
					"name": "全部",
					"submenu": [
						{
						  "name": "MySQL",
						},
						{
						  "name": "Oracle",
						}
					]
				}]
			},
		]
	},
	{
		// name:'附近',
		"type": 'hierarchy',
		"submenu": [{
				"name": '最新',
				"submenu": [{
						"name": "最新"
					},
					{
						"name": "JAVA"
					},
					{
						"name": "PHP"
					},
					{
						"name": "GO"
					},
					{
						"name": "Python"
					},
					{
						"name": "Spark"
					}

				]
			},
			{
				"name": '最热',
				"submenu": [{
						"name": "最热"
					},
					{
						"name": "SQL"
					},
					{
						"name": "JSP"
					},
					{
						"name": "C++"
					},
					{
						"name": "Unity 3D"
					},
					{
						"name": "H5"
					}
				]
			}
		]
	},
	{
		// name:'智能排序',
		"type": 'hierarchy',
		"submenu": [{
				"name": "综合排序"
			},
			{
				"name": "信用排序"
			},
			{
				"name": "价格降序"
			},
			{
				"name": "价格升序"
			}
		]
	},
	{
		// name:'筛选',
		"type": 'filter',
		"submenu": [{
				"name": "优惠",
				"submenu": [{
						"name": "满减活动"
					},
					{
						"name": "打折优惠"
					},
					{
						"name": "会员专享"
					}
				]
			},
			{
				"name": "服务",
				"submenu": [{
						"name": "预定"
					},
					// {
					// 	"name": "点餐"
					// },
					// {
					// 	"name": "外卖"
					// },
					// {
					// 	"name": "WIFI"
					// },
					// {
					// 	"name": "停车位"
					// },
					// {
					// 	"name": "无烟区"
					// },
					// {
					// 	"name": "包厢"
					// },
					{
						"name": "营业中"
					}
				]
			},
			{
				"name": "价格",
				"submenu": [{
						"name": "50以下"
					},
					{
						"name": "50-100"
					},
					// {
					// 	"name": "100-300"
					// },
					{
						"name": "300以上"
					}
				]
			}
		]
	}
]
