export default [
						{
							"post_id": '1',
							"uid": 1,
							"username": "CSDN @张老师🍎🌹💰",
							"header_image": "/static/moments/test/header03.png",
							"content": {
								"text": "从技术上看，大数据与云计算的关系就像一枚硬币的正反面一样密不可分。大数据必然无法用单台的计算机进行处理，必须采用分布式架构。它的特色在于对海量数据进行分布式数据挖掘。但它必须依托云计算的分布式处理、分布式数据库和云存储、虚拟化技术。",
								"images": ["/static/moments/test/rengongzhineng.png"]
							},
							"islike": 0,
							"like": [{
									"uid": 2,
									"username": "编程小菜鸡,"
								},
								{
									"uid": 3,
									"username": "代码推土机"
								}
							],
							"comments": {
								"total": 2,
								"comment": [{
										"uid": 2,
										"username": '小爱',
										"content": "加个微信吧!🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓🍅🍆🌽🍄🌰🍞🍖🍗🍔🍟🍕🍳🍲🍱🍘🍙🍚🍛🍜🍝🍠🍢🍣🍤🍥🍡🍦🍧🍨🍩🍪🎂🍰🍫🍬🍭🍮🍯🍼☕🍵🍶🍷🍸🍹🍺🍻🍴"									},
									{
										"uid": 3,
										"username": '小虎',
										"content": "一起出去好吗?测试1测试2测试3测试4测试5测试6测试7测试8测试9测试10"
									}
								]
							},
							"timestamp": "5分钟前"
						},
						
						
						
						{
							"post_id": 2,
							"uid": 1,
							"username": "spark开源社区 小吴",
							"header_image": "/static/moments/test/header04.png",
							"content": {
								"text": "租房:东环朝南\n\r2室大衣柜\n\r燃气热水器\n\r5楼采光充足\n\r随时入住",
								"images": [
									"/static/moments/test/pig-01.png", 
									"/static/moments/test/pig-02.png",
									"/static/moments/test/pig-03.png", 
									"/static/moments/test/pig-04.png",
									"/static/moments/test/pig-05.png",
									"/static/moments/test/pig-06.png",
									"/static/moments/test/pig-07.png",
									"/static/moments/test/pig-08.png",
									"/static/moments/test/pig-09.png"
								]
							},
							"islike": 0,
							"like": [{
									"uid": 2,
									"username": "饭后一颗烟,"
								},
								{
									"uid": 3,
									"username": "美酒咖啡"
								}
							],
							"comments": {
								"total": 2,
								"comment": [{
										"uid": 2,
										"username": '小虎',
										"content": "今天好冷啊!"
									},
									{
										"uid": 3,
										"username": '小狼',
										"content": "水果很新鲜~~~🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓!"
									}
								] 
							},
							"timestamp": "1小时前"
						},
						
						
						{
							"post_id": 2,
							"uid": 1,
							"username": "钉钉技术交流群 小豆豆",
							"header_image": "/static/moments/test/header05.png",
							"content": {
								"text": "美食花样多，诱人如北北；迎来小宇宙，幸福两行泪,这可是小必的心声啊～",
								"images": [
									"/static/moments/test/xuexi.png", 
									"/static/moments/test/xuexi.png",
									"/static/moments/test/xuexi.png",
									"/static/moments/test/xuexi.png",
									"/static/moments/test/xuexi.png",
									"/static/moments/test/xuexi.png", 
									"/static/moments/test/xuexi.png"
								]
							},
							"islike": 0,
							"like": [{
									"uid": 2,
									"username": "隔壁老王,"
								},
								{
									"uid": 3,
									"username": "对面王寡妇"
								}
							],
							"comments": {
								"total": 2,
								"comment": [{
										"uid": 2,
										"username": '小虎',
										"content": "烧烤小啤酒,越喝越带劲!"
									},
									{
										"uid": 3,
										"username": '小狼',
										"content": "666🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓!"
									}
								]
							},
							"timestamp": "7小时前"
						}
					]