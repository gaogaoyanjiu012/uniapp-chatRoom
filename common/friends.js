module.exports ={
    "list": [
		{
		    "letter": "↑",
		    "data": [
				{
					"userId": 10000,
					"username":"新的朋友",
					"userSex" : 4,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/new_friends.png"
				},
				{
					"userId": 10001,
					"username":"群组",
					"userSex" : 4,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/qunliao2.png"
				},
				{
					"userId": 10002,
					"username":"标签",
					"userSex" : 4,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/biaoqian2.png"
				},
				{
					"userId": 10003,
					"username":"公众号",
					"userSex" : 4,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/gongzhonghao2.png"
				},
		    ]
		}, 
		{
		    "letter": "⭐",
		    "data": [
				{
					"userId": 10004,
					"username":"标星朋友1",
					"userSex" : 3,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/face/tx09.png"
				},
				{
					"userId": 10005,
					"username":"标星朋友2",
					"userSex" : 3,
					"addr": "",
					"num" : "",
					"remarks": "",
					"wechatMoments":"",
					"faceImg":"/static/img/face/tx10.png"
				},
		    ]
		}, 
	  {
        "letter": "A",
        "data": [
            {
				"userId": 10006,
				"username":"阿玛尼",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx01.png"
			},
			{
				"userId": 10007,
				"username":"阿凡提🍎🌹💰",
				"userSex" : 1,
			    "addr": "北京 昌平区",
			    "num" : "222333444",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx02.png"
			},
			{
				"userId": 10008,
				"username":"阿依土鳖",
				"userSex" : 0,
				"addr": "北京 朝阳区",
				"num" : "333444555",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx03.png"
			},
			{
				"userId": 10009,
				"username":"阿强",
				"userSex" : 1,
				"addr": "北京 回龙观",
				"num" : "666777888",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx04.png"
			},
            
          ]
       }, 
	
	{
        "letter": "B",
        "data": [
			{
				"userId": 10010,
				"username":"保罗",
				"userSex" : 1,
				"addr": "北京 望京西",
				"num" : "777888999",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx05.png"
			},
			{
				"userId": 10011,
				"username":"宝强🍎💰",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx06.png"
			},
			{
				"userId": 10012,
				"username":"笨阿三",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx07.png"
			},
			{
				"userId": 10013,
				"username":"北京 潘安",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx08.png"
			},
        ]
    }, 
	
	{
        "letter": "C",
        "data": [
			{
				"userId": 10014,
				"username":"长白山 浩哥",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
	            "wechatMoments":"",
				"faceImg":"/static/img/face/tx09.png"
			},
			{
				"userId": 10015,
				"username":"昌黎 塔吊司机",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx10.png"
			},
        ]
    }, 
	
	{
        "letter": "D",
        "data": [
			{
				"userId": 10016,
				"username":"电气自动化 小张",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx11.png"
			},
			{
				"userId": 10017,
				"username":"都不是事",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx12.png"
			},
        ]
    }, 
	
	{
        "letter": "E",
        "data": [
			{
				"userId": 10018,
				"username":"饿了吃土~",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx13.png"
			},
			{
				"userId": 10019,
				"username":"二嘎",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx14.png"
			},
        ]
    }, 
	
	{
        "letter": "F",
        "data": [
			{
				"userId": 10020,
				"username":"福特",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx01.png"
			},
			{
				"userId": 10021,
				"username":"福临门",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx02.png"
			},
        ]
    }, 
	
	{
        "letter": "G",
        "data": [
			{
			"userId": 10022,	
				"username":"锅盖",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx03.png"
			},
			{
				"userId": 10023,
				"username":"国杠1573",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx04.png"
			},
        ]
    }, 
	
	{
        "letter": "H",
        "data": [
			{
				"userId": 10024,
				"username":"韩俞",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx05.png"
			},
			{
				"userId": 10025,
				"username":"韩寒",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx06.png"
			},
        ]
    }, 
	
	{
        "letter": "I",
        "data": []
    }, 
	
	{
        "letter": "J",
        "data": [
			{
				"userId": 10026,
				"username":"京油子",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                 "wechatMoments":"",
				"faceImg":"/static/img/face/tx06.png"
			},
			{
				"userId": 10027,
				"username":"经纪人 老吴",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx07.png"
			},
        ]
    }, 
	
	{
        "letter": "K",
        "data": [
			{
				"userId": 10028,
				"username":"ktv 马哥",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx08.png"
			},
			{
				"userId": 10029,
				"username":"狂人 泰山",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx09.png"
			},
        ]
    }, 
	
	{
        "letter": "L",
        "data": [
			{
				"userId": 10030,
				"username":"罗伯特",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx10.png"
			},
			{
				"userId": 10031,
				"username":"兰博基尼",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx11.png"
			},
        ]
    }, 
	
	{
        "letter": "M",
        "data": [
			{
				"userId": 10032,
				"username":"满屋飘香",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx12.png"
			},
			{
				"userId": 10033,
				"username":"美丽大脚丫",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx13.png"
			},
        ]
    },
	
	{
        "letter": "N",
        "data": [
			{
				"userId": 10034,
				"username":"你的钱办你的事",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx01.png"
			},
			{
				"userId": 10035,
				"username":"牛人 才哥",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                "wechatMoments":"",
				"faceImg":"/static/img/face/tx02.png"
			},
        ]
    }, 
	
	{
        "letter": "O",
        "data": []
    }, 
	
	{
        "letter": "P",
        "data": [
			{
				"userId": 10036,
				"username":"脾气吼不住",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                "wechatMoments":"",
				"faceImg":"/static/img/face/tx03.png"
			},
			
        ]
    }, 
	
	{
        "letter": "Q",
        "data": [
			{
				"userId": 10037,
				"username":"全都不是事",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx04.png"
			},
			{
				"userId": 10038,
				"username":"秦皇岛 老李头",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx05.png"
			},
        ]
    }, 
	
	{
        "letter": "R",
        "data": [
			{
				"userId": 10039,
				"username":"人间天堂",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx06.png"
			},
        ]
    },
	
	{
        "letter": "S",
        "data": [
			{
				"userId": 10040,
				"username":"三天没吃肉",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx07.png"
			},
			{
				"userId": 10041,
				"username":"苏乞儿",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx08.png"
			},
        ]
    },
	
	{
        "letter": "T",
        "data": [
			{
				"userId": 10042,
				"username":"土豆一码斯",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx09.png"
			},
			{
				"userId": 10043,
				"username":"天涯与你一起闯",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
	            "wechatMoments":"",
				"faceImg":"/static/img/face/tx10.png"
			},
        ]
    },
	
	{
        "letter": "U",
        "data": []
    }, 
	
	{
        "letter": "V",
        "data": []
    }, 
	
	{
        "letter": "W",
        "data": [
			{
				"userId": 10044,
				"username":"王世元",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx01.png"
			},
			{
				"userId": 10045,
				"username":"王文顺",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx02.png"
			},
        ]
    }, 
	
	{
        "letter": "X",
        "data": [
			{
				"userId": 10046,
				"username":"西门大官人",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx03.png"
			},
			{
				"userId": 10047,
				"username":"西施",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                 "wechatMoments":"",			     
				"faceImg":"/static/img/face/tx04.png"
			},
        ]
    }, 
	
	{
        "letter": "Y",
        "data": [
			{
				"userId": 10048,
				"username":"杨戬",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                "wechatMoments":"",
				"faceImg":"/static/img/face/tx05.png"
			},
			{
				"userId": 10049,
				"username":"杨玉环",
				"userSex" : 0,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx06.png"
			},
        ]
    }, 
	
	{
        "letter": "Z",
        "data": [
			{
				"userId": 10050,
				"username":"张三丰",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx07.png"
			},
			{
				"userId": 10051,
				"username":"诸葛亮",
				"userSex" : 1,
				"addr": "北京 海淀区",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx08.png"
			},
        ]
    },
	{
	    "letter": "#",
	    "data": [
			{
				"userId": 10052,
				"username":"333薇薇",
				"userSex" : 1,
				"addr": "马来西亚",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
                "wechatMoments":"",
				"faceImg":"/static/img/face/tx07.png"
			},
			{
				"userId": 10053,
				"username":"🍎乐乐",
				"userSex" : 0,
				"addr": "拉斯维加斯",
				"num" : "111222333",
				"remarks": "有事情打电话:13133338888",
				"wechatMoments":"",
				"faceImg":"/static/img/face/tx08.png"
			},
	    ]
	}
	]
}
