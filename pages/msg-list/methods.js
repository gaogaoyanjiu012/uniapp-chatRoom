
export default {

	PageInit: function(that, Request_1, helper) {
	},

	onNavigationBarButtonTap: function(that, e, helper, Config, Request_1, AppChat) {
		switch (e.index) {
			case 0:
			    // 扫码
				uni.scanCode({
					success: function(res) {

						console.log(res.result)

						var data = JSON.parse(res.result);
						switch (data.type) {
							case 'adduser':
								var item = data.data;
								new Request_1.Request(Config.GlobalSocketUser.AddMember, {
										UserId: Config.UserInfo().id,
										parameter: JSON.stringify({
											userkey: item.id,
											userName: item.info.userName,
											headImg: item.info.headImg,
											ori: '二维码分享'
										}),
										title: '',
										ProjectToken: Config.UserInfo().projectToken
									})
									.Then(a => {
										helper.SetRedDot(true, 1);

										uni.showToast({
											title: '添加好友成功，请查看通讯录'
										});
									})
									.Post();
								break;
							case 'qrlogin':

								AppChat.TaskLogin(that, {
									content: 'open',
									sesstion: data.sesstion
								});

								uni.showModal({
									title: '用户提醒',
									content: '点击确认登陆信息？',
									success: function(res) {

										if (res.confirm) {

											var userInfo = Config.UserInfo();

											AppChat.TaskLogin(that, {
												content: JSON.stringify(userInfo),
												sesstion: data.sesstion
											});

										} else if (res.cancel) {

											AppChat.TaskLogin(that, {
												content: 'close',
												sesstion: data.sesstion
											});

										}
									}
								});

								break;

							case 'addgroup':
								var item = data.data;
								var userInfo = Config.UserInfo();
								new Request_1.Request(Config.GlobalSocketUser.AddGroup, {
										UserId: userInfo.id,
										Level: 3,
										GroupKey: item.id,
										Paras: JSON.stringify({
											userName: userInfo.paras.userName,
											headImg: userInfo.paras.headImg,
											ori: '二维码分享'
										}),
										ProjectToken: Config.UserInfo().projectToken
									})
									.Then(a => {
										helper.SetRedDot(true, 1);

										console.log(JSON.stringify(a))

										uni.showToast({
											title: '添加群组成功，请查看通讯录'
										});
									})
									.Post();
								break;

							default:
								break;
						}
					},
					fail(e) {
						console.log(123);

						console.log(JSON.stringify(e));
					}
				});
				break;
			case 1:
			    // 搜索
				// helper.SetRedDot(false, 1);
				// uni.navigateTo({
				// 	url: '../tree/tree'
				// });
				
				// uni.navigateTo({
				// 	url: '/pages/search/search'
				// });
				
				uni.navigateTo({
					url: '/pages/HM-search/HM-search'
				});
				
				break;
			default:
				break;
		}

	}
};
