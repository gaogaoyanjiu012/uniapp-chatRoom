export default {
	openNav: true,
	isHide: false,
	sesstion: '0',
	
	
	swipeList: [
		{
			id: 0,
			content: '用户0',
			options: [
				{
				  text: '置顶'
				}, 
				{
				  text: '标记已读',
					style: {
						backgroundColor: 'rgb(254,156,1)'
					}
				}, 
				{
					text: '删除',
					style: {
						backgroundColor: 'rgb(255,58,49)'
					}
				}
			]
		},
		{
			id: 1,
			content: '用户1',
			options: [
				{
				  text: '置顶'
				}, 
				{
				  text: '标记已读',
					style: {
						backgroundColor: 'rgb(254,156,1)'
					}
				}, 
				{
					text: '删除',
					style: {
						backgroundColor: 'rgb(255,58,49)'
					}
				}
			]
		},
		{
			id: 2,
			content: '用户2',
			options: [
				{
				  text: '置顶'
				}, 
				{
				  text: '标记已读',
					style: {
						backgroundColor: 'rgb(254,156,1)'
					}
				}, 
				{
					text: '删除',
					style: {
						backgroundColor: 'rgb(255,58,49)'
					}
				}
			]
		}
	],
                value1:false,
				x:0,
				y:0,
				data1:[
					{
						id: 0,
						title:'创建群聊',
						icon:'/static/popups/chuangjianqunliao-lan.png',
						disabled: false
					},
					{
						id: 1,
						title:'加好友/群',
						icon:'/static/popups/tianjiahaoyou.png',
						disabled: false
					},
					{
						id: 2,
						title:'扫一扫',
						icon:'/static/popups/scan_icon.png',
						disabled: false
					},
					{
						id: 3,
						title:'面对面快传',
						icon:'/static/popups/zhifeiji.png',
						disabled: true
					},
					{
						id: 4,
						title:'收付款',
						icon:'/static/popups/shoufukuan.png',
						disabled: true
					}
				],
	demo: {
		openmenuBar: false
	},
	
	applist:[],

	userKey: '',
	headImg: '',

	islink: false,
	userId: 0,
	heartbeattime: '',
	heartbeatintervaltime: 0,
	clientid: '',

	minHeight: '0',
	winHeight: '0',
	overflow:'', // hidden

	directionStr: '水平',
	horizontal: 'right',
	vertical: 'bottom',
	direction: 'vertical',

	pattern: {
		color: '#7A7E83',
		backgroundColor: '#fff',
		selectedColor: '#007AFF',
		buttonColor: '#007AFF'
	},
	content: [{
		iconPath: '/static/img/face.jpg',
		selectedIconPath: '/static/img/face.jpg',
		text: '健健',
		active: false
	}],
	options: [{
			text: '已读',
			style: {
				backgroundColor: '#C7C6CD'
			}
		},
		{
			text: '删除',
			style: {
				backgroundColor: '#dd524d'
			}
		}
	],
	messages: [
		   {
				"title": "系统消息",
				"userSex":1,
				"url": ["/static/img/sys0.png"],
				"message": "版本升级通知",
				"time": "20:4",
				"count": 1,
				"stick": false,
				"key": "115",
				"type": "text",
				"disabled": false
			},
			{
				"title": "A1",
				"userSex":0, // 男
				"url": ["/static/img/face/tx01.png"],
				"message": "吃饭了么",
				"time": "20:4",
				"count": 1,
				"stick": false,
				"key": "116",
				"type": "text",
				"disabled": false
			},
			{
				"title": "B2", 
				"userSex":1, // 女
				"url": ["/static/img/face/tx02.png"],
				"message": "没吃呢",
				"time": "20:4",
				"count": 2,
				"stick": false,
				"key": "117",
				"type": "text",
				"disabled": false
			},
			{
				"title": "C3",
				"userSex":0,
				"url": ["/static/img/face/tx03.png"],
				"message": "🍎🍎🍎啤酒烧烤 去不",
				"time": "20:4",
				"count": 3,
				"stick": false,
				"key": "118",
				"type": "text",
				"disabled": false
			},
			{
				"title": "D4",
				"userSex":1,
				"url": ["/static/img/face/tx04.png"],
				"message": "走啊",
				"time": "20:4",
				"count": 4,
				"stick": false,
				"key": "119",
				"type": "text",
				"disabled": false
			},
			{
				"title": "E5",
				"userSex":0,
				"url": ["/static/img/face/tx05.png"],
				"message": "王亮烧烤 走起啊",
				"time": "20:4",
				"count": 5,
				"stick": false,
				"key": "120",
				"type": "text",
				"disabled": false
		     },
			 {
			 	"title": "F6💰💰💰",
				"userSex":1,
			 	"url": ["/static/img/face/tx06.png"],
			 	// 仅支持网络图片
				"message": "味道真不错,以后常来啊!!! " +
				           // "<img style='width:20px;height:20px;' src='../../static/img/ad-images/sk1.jpg'>"+
						   // "<img style='width:20px;height:20px;' src='../../static/img/ad-images/sk2.jpg'>"+
						   // "<img style='width:20px;height:20px;' src='../../static/img/ad-images/sk3.jpg'>"+
						   // "<img style='width:20px;height:20px;' src='../../static/img/ad-images/sk4.jpg'>"+
						   
						   "<img style='width:20px;height:20px;' src='https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1575951638&di=fa44d112cc0585381d2dbf755469d9dd&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.rmb.bdstatic.com%2Ff2d19ebd94f6f7eedaf89dcb04d5dfbd.jpeg%40wm_2%2Ct_55m%2B5a625Y%2B3L%2BW4uOWIqeivtOWoseS5kA%3D%3D%2Cfc_ffffff%2Cff_U2ltSGVp%2Csz_93%2Cx_59%2Cy_59'>"+
						   "<img style='width:20px;height:20px;' src='https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1575951638&di=fa44d112cc0585381d2dbf755469d9dd&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.rmb.bdstatic.com%2Ff2d19ebd94f6f7eedaf89dcb04d5dfbd.jpeg%40wm_2%2Ct_55m%2B5a625Y%2B3L%2BW4uOWIqeivtOWoseS5kA%3D%3D%2Cfc_ffffff%2Cff_U2ltSGVp%2Csz_93%2Cx_59%2Cy_59'>"+
						   "<img style='width:20px;height:20px;' src='https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1575951638&di=fa44d112cc0585381d2dbf755469d9dd&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.rmb.bdstatic.com%2Ff2d19ebd94f6f7eedaf89dcb04d5dfbd.jpeg%40wm_2%2Ct_55m%2B5a625Y%2B3L%2BW4uOWIqeivtOWoseS5kA%3D%3D%2Cfc_ffffff%2Cff_U2ltSGVp%2Csz_93%2Cx_59%2Cy_59'>"+
						   "<img style='width:20px;height:20px;' src='https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1575951638&di=fa44d112cc0585381d2dbf755469d9dd&imgtype=jpg&er=1&src=http%3A%2F%2Fpic.rmb.bdstatic.com%2Ff2d19ebd94f6f7eedaf89dcb04d5dfbd.jpeg%40wm_2%2Ct_55m%2B5a625Y%2B3L%2BW4uOWIqeivtOWoseS5kA%3D%3D%2Cfc_ffffff%2Cff_U2ltSGVp%2Csz_93%2Cx_59%2Cy_59'>"+
						   "",
			 	"time": "20:4",
			 	"count": 5,
			 	"stick": false,
			 	"key": "120",
			 	"type": "text",
			 	"disabled": false
			  },
			  {
			  	"title": "G7",
				"userSex":0,
			  	"url": ["/static/img/face/tx07.png"],
			  	"message": "饭后一颗烟",
			  	"time": "20:4",
			  	"count": 5,
			  	"stick": false,
			  	"key": "120",
			  	"type": "text",
			  	"disabled": false
			   },
			   {
			   	"title": "H8",
				"userSex":1,
			   	"url": ["/static/img/face/tx08.png"],
			   	"message": "赛过活神仙",
			   	"time": "20:4",
			   	"count": 5,
			   	"stick": false,
			   	"key": "120",
			   	"type": "text",
			   	"disabled": false
			    },
				{
					"title": "I9",
					"userSex":0,
					"url": ["/static/img/face/tx09.png"],
					"message": "我送你回家啊",
					"time": "20:4",
					"count": 5,
					"stick": false,
					"key": "120",
					"type": "text",
					"disabled": false
				 },
				 {
				 	"title": "J10",
					"userSex":1,
				 	"url": ["/static/img/face/tx10.png"],
				 	"message": "不用了,我打个滴滴",
				 	"time": "20:4",
				 	"count": 5,
				 	"stick": false,
				 	"key": "120",
				 	"type": "text",
				 	"disabled": false
				  },
				  {
				  	"title": "K11",
					"userSex":0,
				  	"url": ["/static/img/face/tx11.png"],
				  	"message": "有机会在聚",
				  	"time": "20:4",
				  	"count": 5,
				  	"stick": false,
				  	"key": "120",
				  	"type": "text",
				  	"disabled": false
				   },
				   {
				   	"title": "L12",
					"userSex":1,
				   	"url": ["/static/img/face/tx12.png"],
				   	"message": "OK 有事打电话",
				   	"time": "20:4",
				   	"count": 5,
				   	"stick": false,
				   	"key": "120",
				   	"type": "text",
				   	"disabled": false
				    }
					
	],
	
	
};
