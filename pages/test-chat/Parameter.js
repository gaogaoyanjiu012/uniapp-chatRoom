export default {
	key: '0',
	//文字消息
	textMsg: '',
	//消息列表
	isHistoryLoading: false,
	scrollAnimation: false,
	scrollTop: 0,
	scrollToView: '',
	msgList: [],
	msgImgList: [],
	myuid: 0,

	//录音相关参数
	// #ifndef H5
	//H5不能录音
	RECORDER: uni.getRecorderManager(),
	// #endif
	isVoice: false,
	voiceTis: '按住 说话',
	recordTis: '手指上滑 取消发送',
	recording: false,
	willStop: false,
	initPoint: {
		identifier: 0,
		Y: 0
	},
	recordTimer: null,
	recordLength: 0,

	//播放语音相关参数
	AUDIO: uni.createInnerAudioContext(),
	playMsgid: null,
	VoiceTimer: null,
	// 抽屉参数
	popupLayerClass: '',
	// more参数
	hideMore: true,
	//表情定义
	hideEmoji: true,
	emojiList: parameter.emojiList,
	onlineEmoji: parameter.onlineEmoji,
	//红包相关参数
	windowsState: '',
	redenvelopeData: {
		rid: null, //红包ID
		from: null,
		face: null,
		blessing: null,
		money: null
	},
	
	taskUserInfo: {
		userName: '',
		headImg: ''
	},
	
	isGroup: false
};
