
var videoData = [{
		"srcList": [{
			"title": "高清",
			"src": "http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8"
		}],
		"danmuList": [{
				"text": "💰 CCTV-1 直播 ",
				"color": "#f00",
				"time": 1,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹 有好看的电视剧么",
				"color": "#00f",
				"time": 2,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎 最近很无聊啊 ",
				"color": "#ff0",
				"time": 3,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "CCTV-1 直播",
		"initialTime": 0,
		"duration": 60,
		"download": true,
		"audio": "/static/audio.mp3"
	},

	{
		"srcList": [{
				"title": "流畅",
				"src": "http://1252463788.vod2.myqcloud.com/95576ef5vodtransgzp1252463788/e1ab85305285890781763144364/v.f10.mp4"
			},
			{
				"title": "标清",
				"src": "http://1252463788.vod2.myqcloud.com/95576ef5vodtransgzp1252463788/e1ab85305285890781763144364/v.f20.mp4"
			},
			{
				"title": "高清",
				"src": "http://1252463788.vod2.myqcloud.com/95576ef5vodtransgzp1252463788/e1ab85305285890781763144364/v.f30.mp4"
			}
		],

		"danmuList": [{
				"text": "💰💰 云端科技!!!",
				"color": "#f00",
				"time": 2,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹 技术革命!!!",
				"color": "#00f",
				"time": 3,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎 大数据时代!!!",
				"color": "#ff0",
				"time": 4,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "腾讯云,科技引领未来!!!",
		"duration": 60,
		"initialTime": 0,
		"download": false,
		"task": {}
	},
	{
		"srcList": [{
				"title": "流畅",
				"src": "/static/video/GL9-51CPU-1-LQ.mp4"
			},
			{
				"title": "标清",
				"src": "/static/video/GL9-51CPU-1-BQ.mp4"
			},
			{
				"title": "高清",
				"src": "/static/video/GL9-51CPU-1-HQ.mp4"
			}
		],
		"danmuList": [{
				"text": "💰💰💰 51CPU 单片机 ",
				"color": "#f00",
				"time": 1,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹🌹 二月红前来报道!!! ",
				"color": "#00f",
				"time": 2,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎🍎 小板凳准备好啦!!! ",
				"color": "#ff0",
				"time": 3,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "第01课 51CPU 单片机",
		"duration": 60,
		"initialTime": 0,
		"download": false,
		"task": {}
	},
	{
		"srcList": [{
				"title": "流畅",
				"src": "/static/video/GL9-51CPU-2-LQ.mp4"
			},
			{
				"title": "标清",
				"src": "/static/video/GL9-51CPU-2-BQ.mp4"
			},
			{
				"title": "高清",
				"src": "/static/video/GL9-51CPU-2-HQ.mp4"
			}
		],
		"danmuList": [{
				"text": "💰💰💰💰 整点开播 ",
				"color": "#f00",
				"time": 2,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹🌹🌹 听懂了请扣1 ",
				"color": "#00f",
				"time": 3,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎🍎🍎 禁止刷屏! ",
				"color": "#ff0",
				"time": 4,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "第02课 前言",
		"duration": 80,
		"initialTime": 0,
		"download": false,
		"task": {}
	},
	{
		"srcList": [{
			"title": "流畅",
			"src": "/static/video/GL9-51CPU-3-LQ.mp4"
		}],
		"danmuList": [{
				"text": "💰💰💰💰💰 核心介绍 ",
				"color": "#f00",
				"time": 1,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹🌹🌹🌹 看起来不错 ",
				"color": "#00f",
				"time": 3,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎🍎🍎🍎 菜鸟跟着学编程 ",
				"color": "#ff0",
				"time": 3,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "第03课 单片机 课程介绍",
		"duration": 75,
		"initialTime": 0,
		"download": false,
		"task": {}
	},
	{
		"srcList": [{
			"title": "标清",
			"src": "/static/video/GL9-51CPU-4-BQ.mp4"
		}],
		"danmuList": [{
				"text": "💰💰💰💰💰💰 安装方便 ",
				"color": "#f00",
				"time": 1,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹🌹🌹🌹🌹 思路清晰 ",
				"color": "#00f",
				"time": 2,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎🍎🍎🍎🍎 通熟易懂 ",
				"color": "#ff0",
				"time": 2,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "第04课 开发板 独立模块",
		"duration": 80,
		"initialTime": 0,
		"download": false,
		"task": {}
	},
	{
		"srcList": [{
			"title": "高清",
			"src": "/static/video/GL9-51CPU-5-HQ.mp4"
		}],
		"danmuList": [{
				"text": "💰💰💰💰💰💰 5伏电源 ",
				"color": "#f00",
				"time": 2,
				"avatar": "/static/img/face/tx01.png"
			},
			{
				"text": "🌹🌹🌹🌹🌹🌹 二极管、数码管 ",
				"color": "#00f",
				"time": 3,
				"avatar": "/static/img/face/tx02.png"
			},
			{
				"text": "🍎🍎🍎🍎🍎🍎 安排作业 ",
				"color": "#ff0",
				"time": 3,
				"avatar": "/static/img/face/tx03.png"
			}
		],
		"title": "第05课 开发板 功能详情",
		"duration": 70,
		"initialTime": 0,
		"download": false,
		"task": {}
	}
]




// export与export default均可用于导出常量、函数、文件、模块等
// 在一个文件或模块中，export、import可以有多个，export default仅有一个
// 通过export方式导出，在导入时要加{ }，export default则不需要
// export能直接导出变量表达式，export default不行。


export default videoData;